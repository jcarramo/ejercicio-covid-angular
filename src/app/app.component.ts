import { Component, OnInit } from '@angular/core';

import { CovidService } from './services/covid.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'covid';

  arrCountries = [];
  arrSpain = [];  
  objGlobal = {};

  showAll: boolean = true;
  showGlobal: boolean = true;


  isShow: boolean = true;


  constructor( public covidService: CovidService ) {}

  ngOnInit(): void {
    this.covidService.getCovids()
      .subscribe(
        response => {
          this.arrCountries = response;
        },
        error =>  console.log(error)
      );
  
    this.covidService.getGeneral()
      .subscribe(
        response => {
          this.objGlobal = response;
        },
        error =>  console.log(error)
      );

      this.covidService.getSpain()
      .subscribe(
        response => {
          this.arrSpain = response;
        },
        error =>  console.log(error)
      );
  }

  // Manera Valida

  // toggleAll(): void{
  //   this.showAll = true;
  //   this.showGlobal = true;
  // }

  // toggleGlobal(){
  //   this.showAll  = false;
  //   this.showGlobal = false;
  // }

  toggleShow(){
    this.isShow = !this.isShow
  }

}
