import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
 
  providedIn: 'root'
})
export class CovidService {

  constructor( private http: HttpClient ) { }

  getCovids(): Observable<any>{
    const json = 'https://api.covid19api.com/summary';
    return this.http.get(json)
      .pipe( map( data => {
        return data['Countries'];
      }))
  };

  getGeneral(): Observable<any>{
    const json = 'https://api.covid19api.com/summary';
    return this.http.get(json)
      .pipe(map(data => {
        return data['Global'];
      }));
  }

  getSpain(): Observable<any>{
    const json = 'https://api.covid19api.com/summary';
    return this.http.get(json)
      .pipe(map(data => {
        return data['Countries'].filter( ( country: any ) => country['Country'] === 'Spain')
      }))
    
  }
}
